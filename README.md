# Package Installation

```
python setup.py sdist
pip install djangocms-workflows-<version>.tar.gz
```
Add two apps to INSTALLED_APPS:

```
INSTALLED_APPS = [
...
'workflows',
'adminsortable2',
...
```
